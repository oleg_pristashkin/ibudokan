<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecommendedToCourses extends Migration
{
    public function up()
    {
        Schema::table('courses', function($table) {
            $table->boolean('recommended')->default(false);
        });
    }
    public function down()
    {
        Schema::table('courses', function($table) {
            $table->dropColumn('recommended');
        });
    }
}
