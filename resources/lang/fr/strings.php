<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strings Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in strings throughout the system.
    | Regardless where it is placed, a string can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'adsense_ad_client' => 'Adsense Ad Client (eg. ca-pub-xxxx)',
        'adsense_sidebar_responsive_slot' => 'Adsense vertical ad slot (300x600)',
        'adsense_top_responsive_slot' => 'Adsense horizontal ad slot (728x90)',
        'max-video-upload-video' => 'Upload - Taille max. de la vidéo',
        'close' => 'Fermer',
        'update' => 'Update',
        'edit' => 'Editer',
        'view' => 'Voir',
        'delete' => 'Effacer',
        'name' => 'Nom',
        'add' => 'Ajouter',
        'manage-blog' => 'Gérer les pages et les blogs',
        'create-new' => 'Créer un nouveau',
        'id' => 'ID',
        'are-you-sure' => 'Etes-vous sûr?',
        'unable-to-recover' => 'Vous ne pourrez pas récupérer ceci!',
        'yes-delete' => 'Oui, effacer.',
        'cancel' => 'Annuler',
        'deleted' => 'Effacé',
        'cancelled' => 'Annulé',
        'no-of-posts' => 'Nombre de posts',
        'no-of-courses' => 'Nombre de cours',
        'manage-messages' => 'Gérer les messages',
        'subject' => 'Sujet',
        'send-to' => 'Envoyer à',
        'everyone' => 'Tout le monde',
        'administrators' => 'Administrateurs',
        'authors' => 'Auteurs',
        'inactive-users' => 'Utilisateur inactif',
        'all-students' => 'Tous les élèves',
        'selected-users' => 'Utilisateurs selectionnés',
        'recipients' => 'Destinataires',
        'body' => 'Body',
        'all' => 'Tous',
        'draft-messages' => 'Brouillons',
        'sent-messages' => 'Messages envoyés',
        'status' => 'Statut',
        'created-on' => 'Crée le',
        'sent-on' => 'Envoyé le',
        'send' => 'Envoyer',
        'title' => 'Titre',
        'permalink' => 'Permalink',
        'type' => 'Type',
        'category' => 'Categorie',
        'language' => 'Langage',
        'published' => 'Publié',
        'display-main-menu' => 'Montrer le lien dans le menu principal',
        'display-footer' => 'Montrer le lien dans le bas de page',
        'article' => 'Article',
        'page' => 'Page',
        'site-pages' => 'Pages du site',
        'blog-posts' => 'Blog Posts',
        'submitted' => 'Présenté',
        'processing' => 'En cours de traitement',
        'processed' => 'Traité',
        'any-comment' => 'Des commentaires?',
        'manage-environment-variables' => 'Gérer les variables environnement',
        'site-settings' => 'Paramètres du Site',
        'site-info' => 'Infos du site Info',
        'send-email-notification' => 'Envoyer des notifications par email aux utilisateurs?',
        'email-notification-help' => 'Verifier vos paramètres d\'email si vous avez choisi Oui',
        'site-description' => 'Description du site',
        'theme-color' => 'Couleur du thème',
        'green' => 'Vert',
        'blue' => 'Bleu',
        'video-upload-location' => 'Destination des vidéos uploadées',
        'video-upload-help' => 'Choisissez où vos vidéos uploadées vont être sauvegardé. TutorPro accepte le stockage local Amazon S3.',
        'save-on-local-server' => 'Sauvegarder sur le serveur local',
        'save-on-amazon' => 'Sauvegarder sur Amazon S3',
        'amazon-help' => 'Si vous selectionnez Amazon S3, entrez vos identifiants Amazon S3 dans les paramètres .env',
        'google-analytics-code' => 'Google Analytics Code',
        'site-currency-code' => 'Code de la monnaie du site',
        'site-currency-symbol' => 'Symbole de la monnaie du site',
        'position-of-currency-symbol' => 'Position du symbole de la monnaie',
        'to-the-left' => 'A gauche de la valeur (front)',
        'to-the-right' => 'A droite de la valeur (back)',
        'sales-and-commission' => 'Ventes et Commissions',
        'author-percentage' => 'Pourcentage de l\'auteur sur les ventes organiques',
        'author-percentage-promotion' => 'Pourcentage de l\'auteur sur ses promotions',
        'social-network-accounts' => 'Comptes Reseaux sociaux',
        'contact-email' => 'Email de contact',
        'save' => 'Sauver',
        'revenue' => 'Revenues des ventes',
        'withdrawals' => 'Retraits',
        'lifetime-sales-amount' => 'Montant total des ventes',
        'lifetime-sales-quantity' => 'Nombre total de cours vendus',
        'this-month-sales-amount' => 'Montant des ventes du mois en cours',
        'this-month-sales-quantity' => 'Nombre de cours vendus du mois en cours',
        'lifetime-withdrawal-amount' => 'Montant total des retraits',
        'lifetime-withdrawal' => 'Nombre total de retraits',
        'this-month-withdrawal-amount' => 'Montant des retraits du mois en cours',
        'this-month-withdrawals' => 'Nombre de retraits du mois en cours',
        'pending-withdrawals' => 'Retraits en attente',
        'total-withdrawals-processed' => 'Nombre total de retraits effectués',
        'total-withdrawals-not-processed' => 'Nombre de retraits non effectués',
        'withdrawal' => 'Retrait',
        'sale' => 'Vente',
        'code' => 'Code',
        'percent-off' => 'Pourcentage off',
        'quantity' => 'Quantité',
        'expires' => 'Date d\'expiration',
        'coupons' => 'Gerer les coupons',
        'coupons-small' => 'Ces coupons seront utilisables pour tous les cours et sur tout le site. Un seul coupon peut être actif.',
        'activate' => 'Activer',
        'deactivate' => 'Désactiver',
        'active' => 'Actif',
        'inactive' => 'Inactif',
        'recipient-empty' => 'La liste des bénéficiaires est vide. Changez de groupe de bénéficiaire et essayer de nouveau.',
        'cannot-activate-expired'=> 'The coupon a expiré. Il ne peut pas être activé',
        'cannot-activate-exhausted' => 'Ce coupon a été utilisé au maximum. Il ne peut pas être réactivé.',
        'redeemed' => 'Number redeemed',
        'allow-video-upload' => 'Autoriser l\'upload des videos?',
        'allow-youtube-video' => 'Autoriser les auteurs à utiliser des vidéos de Youtube?',
        'allow-vimeo-video' => 'Autoriser les auteurs à utiliser des vidéos de Vimeo?',
        'allow-s3-video' => 'Autoriser les auteurs à utiliser des vidéos de Amazon S3?',
        'true' => 'Vrai',
        'false' => 'Faux',
        'pricelist' => 'Liste de prix des cours',
        'pricelist-help' => 'Liste séparée par des virgules. Ceci est la liste des prix que les instructeurs doivent selectionner pour leur cours. Ne pas inclure un prix égal à zéro, car il est ajouté par défaut.',
        'set-same-permalink' => 'Assurer vous que le permalink est identique pour le même article dans les différentes langues. Par exemple, si vous avez créee une page <b>"about-us"</b> en anglais et en espagnol, elles doivent avoir le même permalink <b>"about-us"</b>',
    
        'course-has-no-preview' => 'L\auteur n\'a pas sélectionner d\'aperçu gratuit. Ne confirmez pas ce cours et demandez à l\'auteur de choisir une vidéo comme aperçu gratuit',
        'featured-frontend' => 'Présent sur la page principale',
    
        'affiliate-settings' => 'Paramètre affilié',
        'affiliate-percentage' => 'Pourcentage affilié',
        'affiliate-percentage-help' => 'Pourcentage reversé aux affiliés pour promouvoir un cours. L\'auteur et le site partageront le solde après que le pourcentage de l\'affilié ait été déduit.',
        'affiliate-cookie-lifetime' => 'Durée de vie du cookie de l\'affilié (en minutes)',
        'affiliate-cookie-help' => 'Pendant combien de temps le lien de référence doit-il être valide? Le système enregistre le lien de référence dans un cookie du navigateur, merci de préciser qunad vous désirez que ce cookie expire',
        'author-minimum-payout-amount' => 'Montant de retrait minimum',
        'author-minimum-payout-amount-help' => 'Quel est le montant de retrait minimum qu\'un utilisateur peut demander?',
    
                
        'access' => [
            'users' => [
                'delete_user_confirm'  => 'Êtes-vous sûr de vouloir supprimer cet utilisateur de façon permanente ? Toutes  les références à cet utilisateur dans l\'application peuvent provoquer des erreurs et cette opération ne peut être annullée.',
                'if_confirmed_off'     => '(pour le mode sans confirmation)',
                'restore_user_confirm' => 'Restaurer cet utilisateur à son statut original ?',
            ],
        ],

        'dashboard' => [
            'title'   => 'Tableau de bord administrateur',
            'welcome' => 'Bienvenue',
        ],

        'general' => [
            'all_rights_reserved' => 'Tous droits réservés.',
            'are_you_sure'        => 'Etes-vous sûr?',
            'boilerplate_link'    => 'Laravel 5 Boilerplate',
            'continue'            => 'Continuer',
            'member_since'        => 'Membre depuis',
            'minutes'             => ' minutes',
            'search_placeholder'  => 'Rechercher...',
            'timeout'             => 'Vous avez été automatiquement déconnecté pour cause d\'inactivité pendant ',

            'see_all' => [
                'messages'      => 'Voir tous les messages',
                'notifications' => 'Voir toutes les notifications',
                'tasks'         => 'Voir les nouvelles tâches',
            ],

            'status' => [
                'online'  => 'En ligne',
                'offline' => 'Hors ligne',
            ],

            'you_have' => [
                'messages'      => '{0} Vous n\'avez pas de message|{1} Vous avez 1 message|[2,Inf] Vous avez :number messages',
                'notifications' => '{0} Vous n\'avez pas de notification|{1} Vous avez 1 notification|[2,Inf] Vous avez :number notifications',
                'tasks'         => '{0} Vous n\'avez pas de tâche affectée|{1} Vous avez 1 tâche affectée|[2,Inf] Vous avez :number tâches affectées',
            ],
        ],

        'search' => [
            'empty'      => 'Veuillez entrer un terme de recherche.',
            'incomplete' => 'Vous devez mettre en place votre propre logique pour ce système.',
            'title'      => 'Résultats de recherche',
            'results'    => 'Résultats de la recherche :query',
        ],
    
        'yes' => 'Yes',
        'payment_gateways' => 'Payment Gateways',
        'enable_stripe' => 'Activer Stripe',
        'enable_stripe_help' => 'Activer les paiements par Stripe? Ne pas l\'autoriser si Braintree est activé',
        'enable_paypal' => 'Activer PayPal',
        'enable_paypal_help' => 'Activer les paiments par Paypal?Ne pas l\'autoriser si Braintree est activé',
        'enable_braintree' => 'Activer Braintree?',
        'enable_braintree_help' => 'Activer les paiements par Braintree? Braintree combine cartes de crédit et Paypal. Les options cartes de crédit et Paypal doivent alors être désactivées pour utiliser Braintree.',
    
        'enable_omise' => 'Activer Omise?',
        'enable_omise_help' => 'Activer les paiements par Omise?',
    
        'enable_razorpay' => 'Activer RazorPay?',
        'enable_razorpay_help' => 'Activer les paiements par RazorPay? (Inde uniquement)',


        'welcome' => '<p>Le thème AdminLTE est créé par <a href="https://almsaeedstudio.com/" target="_blank">https://almsaeedstudio.com/</a>. Ceci est une version allégée avec seulement les styles et les scripts nécessaires pour le faire fonctionner. Téléchargez la version complète pour commencer à ajouter des composants à votre tableau de bord.</p> <p>Toutes les fonctionnalités sont dédiées à l\'affichage, à l\'exception de la <strong>Gestion des utilisateurs</strong> à gauche. Ce projet boilerplate est livré avec une bibliothèque de contrôle d\'accès entièrement fonctionnelle pour gérer les utilisateurs / rôles / permmissions.</p><p>Gardez à l\'esprit que c\'est un travail en cours et qu\'il peut subsister quelques anomalies ou bugs qui n\'ont pas été corrigés. Je ferai de mon mieux pour y remédier.</p><p>J\'espère que vous apprécierez le travail que j\'ai mis dans ce projet. Visitez la page <a href="https://github.com/rappasoft/laravel-5-boilerplate" target="_blank">GitHub</a> pour plus d\'informations et enregistrez les <a href="https://github.com/rappasoft/Laravel-5-Boilerplate/issues" target="_blank">anomalies ici</a>.</p><p><strong>Ce projet demande beaucoup d\'efforts au vu du rythme soutenu des changements apportés à la branche master de Laravel, toute aide est bienvenue.</strong></p><p>- Anthony Rappa</p>',
    ],

    'emails' => [
        'auth' => [
            'error'                   => 'Oups!',
            'greeting'                => 'Bonjour!',
            'regards'                 => 'Salutations,',
            'trouble_clicking_button' => 'Si vous ne pouvez pas cliquer sur le bouton ":action_text", copiez et collez l\'URL ci-dessous dans un navigateur:',
            'thank_you_for_using_app' => 'Merci d\'utiliser notre application!',

            'password_reset_subject'    => 'Votre lien de réinitialisation',
            'password_cause_of_email'   => 'Vous recevez cet email car nous avons reçu une demande de réinitialisation de mot de passe pour ce compte.',
            'password_if_not_requested' => 'Si vous n\'avez pas effectué cette demande, aucune autre action n\'est requise.',
            'reset_password'            => 'Cliquez ici pour réinitialiser votre mot de passe',

            'click_to_confirm' => 'Cliquez ici pour confirmer votre compte :',
        ],
    ],

    'frontend' => [
        'all-rights-reserved' => 'All Rights Reserved',
        'review' => 'avis',
        'load-more' => 'Charger plus',
        'no-reviews' => 'Pas encore de commentaires',
        'average_rating_from_reviews' => 'Moyenne de :number :review',
        'password-not-required' => 'Vous êtes connecté avec votre compte de réseau social. Ceci ne nécessite pas de mot de passe. Mais si vous voulez en créer un, déconnectez-vous and utilisez le lien "Password Reset" sur la page de connection pour créer un mot de passe.',
    
        'trouble_clicking_button' => 'If you’re having trouble clicking the ":action_text" button, copy and paste the URL below into your web browser:',
        'test' => 'Test',

        'tests' => [
            'based_on' => [
                'permission' => 'Helper sur la base de la permissions : ',
                'role'       => 'Helper sur la base du rôle : ',
            ],

            'js_injected_from_controller' => 'Javascript injecté depuis un contrôleur',

            'using_blade_extensions' => 'Utilisation des extensions Blade',

            'using_access_helper' => [
                'array_permissions'     => 'L\'utilisateur doit disposer de toutes les permissions d\'un tableau, identifiées soit par leur ID, soit par leur nom.',
                'array_permissions_not' => 'L\'utilisateur doit disposer d\'au moins une des permissions d\'un tableau, identifiées soit par leur ID, soit par leur nom.',
                'array_roles'           => 'L\'utilisateur doit disposer de tous les rôles d\'un tableau, identifiés soit par leur ID, soit par leur nom.',
                'array_roles_not'       => 'L\'utilisateur doit disposer d\'au moins un des rôles d\'un tableau, identifiés soit par leur ID, soit par leur nom.',
                'permission_id'         => 'L\'utilisateur doit disposer d\'une permission identifiée par son ID',
                'permission_name'       => 'L\'utilisateur doit disposer d\'une permission identifiée par son nom',
                'role_id'               => 'L\'utilisateur doit disposer d\'un rôle identifié par son ID',
                'role_name'             => 'L\'utilisateur doit disposer d\'un rôle identifié par son nom',
            ],

            'view_console_it_works'          => 'Sur la console du navigateur, vous devriez voir  \'it works!\', ce qui est produit depuis le FrontendController@index',
            'you_can_see_because'            => 'Vous voyez ce message car vous disposez du rôle \':role\'!',
            'you_can_see_because_permission' => 'Vous voyez ce message car vous disposez de la permission \':permission\'!',
        ],

        'user' => [
            'change_email_notice' => 'If you change your e-mail you will be logged out until you confirm your new e-mail address.',
            'email_changed_notice' => 'You must confirm your new e-mail address before you can log in again.',
            'profile_updated'  => 'Profil modifié avec succès.',
            'password_updated' => 'Mot de passe modifié avec succès.',
        ],
        'about-instructor'  => 'A propos de l\'instructeur',
        'advanced'  => 'Avancé',
        'all' => 'Tous',
        'all-categories' => 'Toutes les catégories',
        'all-levels' => 'Tous les niveaux',
        'announcements' => 'Annonce des Cours',
        'answer'    => 'Réponse',
        'answer-delete' => 'Cette réponse va être supprimée définitivement',
        'answered'  => 'Répondu',
        'answers'   => 'Réponses',
        'article'   => 'Article',
        'ask-a-question'    => 'Poser une question',
        'avg-rating' => 'Evaluation moyenne',
        'back-to-course' => 'Retour au cours',
        'back-to-home'  => 'Retour à la page principale',
        'back-to-questions' => 'Retour aux questions',
        'beginner' => 'Débutant',
'browse-our-library' => 'Naviguez dans notre bibliothèque',
'browse-our-library-small' => 'Regardez ce que nos instructeurs vous proposent. Notre bibliothèque s\'agrandit chaque jour.',
        'buy-now'   => 'Acheter maintenant',
        'cancel' => 'Annuler',
        'categories' => 'Categories',
        'comments'  => 'Commentaires',
        'complete'  => 'Complet',
        'content'   => 'Contenu du cours',
        'continue-to-lesson' => 'Continuer la leçon',
        'created-by' => 'Crée par',
        'courses' => 'Cours',
        'delete' => 'Effacer',
        'description'   => 'Description',
        'edit' => 'Editer',
        'enroll-now'    => 'Inscrivez-vous maintenant',
        'enter-comment' => 'Entrer un commentaire',
        'featured-in' => 'Présents dans',
        'feedback'  => 'Commentaires des étudiants',
        'follow-question'   => 'Suivre cette question',
        'free-courses' => 'Cours gratuits',
        'highest-rated' => 'Les mieux notés',
        'intermediate' => 'Intermediaire',
        'level' => 'Niveau',
        'language' => 'La langue',
        'english' => 'Anglais',
        'french' => 'Français',
        'spanish' => 'Espagnol',
        'load-more' => 'Voir plus...',
        'login-to-enroll'   => 'Connectez-vous pour vous inscrire',
        'mark-as-answer' => 'Marqué comme une réponse',
        'mark-all-as-read' => 'Marqué tout comme lu',
        'mark-as-completed' => 'Marqué comme achevé',
        'mark-as-helpful'=> 'Marqué comme utile',
        'mark-as-uncompleted' => 'Marqué comme incomplet',
        'messages' => 'Messages',
        'minutes'   => 'minutes',
        'more'  => 'Plus...',
        'next'  => 'Suivant',
        'no-results'=> 'Résultats trouvés. Totalité affiché',
        'not-rated' => 'Aucune note',
        'oldest-first' => 'Les plus anciens en premier',
        'overview' => 'Description du cours',
        'posted'    => 'Posted',
        'paid-courses' => 'Cours payants',
        'popular-tags' => 'Tags populaires',
        'post'   => 'Post',
        'profile'   => 'Profil',
        'premium-content' => 'Contenu premium',
        'preview'   => 'Aperçu',
        'previous'  => 'Précédent',
        'price' => 'Prix',
        'price-asc' => 'Prix (asc)',
        'price-desc' => 'Prix (desc)',
        'purchases' => 'Historique achat',
        'question-title' => 'Question title',
        'questions' => 'Q&A Forum',
        'read-more' => 'Voir plus',
        'recent-first' => 'Les plus récent en premier',
        'response'  => 'réponse',
        'review-this-course'    => 'Commenter ce cours',
        'search'    => 'Chercher',
        'search-author-or-title' => 'Entrer le titre du cours ou le nom de l\'instructeur',
        'section'   => 'Section',
        'sections'  => 'Sections',
        'send' => 'Envoyer',
        'share-this'    => 'Partager ceci',
        'sort-by' => 'Trier par',
        'start-course'  => 'Commencer le cours',
        'submit' => 'Envoyer',
        'unfollow-question' => 'Ne plus suivre cette question',
        'update' => 'Mettre à jour',
        'update-question' => 'Mettre à jour les questions',
        'updated'   => 'Mis à jour',

    
    
        'welcome_to' => 'Bienvenue sur :place',
    
        'wishlist'  => 'Liste de souhaits',
        'you-marked-as-helpful'=> 'Vous l\'avez noté comme utile',
        'blog' => 'Blog',
        'blog-small' => 'Read Posts from our team, including help tips for students and instructors',
        'start-teaching' => 'Commencez à enseigner',
        'unread' => 'Non lu',
        'student' => 'Eléve',
        'my-courses' => 'Mes Cours',
        'my-enrolled-courses' => 'Mes cours suivis',
        'my-wishlist' => 'Ma liste de souhaits',
        'revenue-report' => 'Rapport des revenus',
        'create-new-course' => 'Créer un nouveau cours',
        'pages' => 'Pages',
        'site-pages' => 'Pages du site',
        'enter-your-review' => 'Entrer votre commentaire',
        'download-lesson-resources' => 'Télécharger les ressources de la leçon',
        'subject' => 'Sujet',
        'enter-your-message' => 'Entrer votre message',
        'you' => 'Vous',
        'send' => 'Envoyé',
        'enter-command-to-send' => 'You can use <code>ctrl+enter</code> on Windows or <code>cmd+enter</code> on Mac to send',
        'confirm-purchase' => 'Confirm your purchase for',
        'you-pay' => 'You Pay',
        'course-price' => 'Course Price',
        'pay-with-paypal' => 'Pay with PayPal',
        'pay-with-creditcard' => 'Pay with Credit Card',
        'go-to-paypal' => 'Go to PayPal',
        'pay' => 'Pay',
        'redeem-coupon' => 'Redeem a coupon',
        'remove-coupon' => 'Remove coupon',
        'apply-coupon' => 'Apply Coupon',
        'enter-code' => 'Enter coupon code',
        'invalid-coupon' => 'Coupon code invalid',
        'applied-coupon' => 'Applied Coupon',
        'exhausted-coupon' => 'Oops too late. Others have claimed this deal.',
        'expired-coupon' => 'This coupon has expired',
        'processing' => 'Processing. Please wait...',
        'new-answer-posted' => 'A new answer was posted to the question',
        'new-announcement-posted' => 'A new announcement was posted in',
        'edit-profile'  => 'Edit Profile',
        'change-password' => 'Change Password',
        'preferences'   => 'Preferences',
        'date' => 'Date',
        'coupon-code' => 'Coupon code',
        'amount-paid' => 'Amount Paid',
        'payment-type' => 'Payment Type',
        'credit-card'  => 'Credit Card',
        'nothing-in-wishlist' => 'No courses in your wishlist yet. You can add courses to your wishlist to easily enroll when you are ready.',
        'browse-and-add-to-wishlist' => 'Browse courses and add them to your wishlist now.',
        'member-since' => 'Member since',
        'total-reviews' => 'Total Reviews',
        'total-courses' => 'Total Courses',
        'total-students' => 'Total Students',
        'average-rating' => 'Average Rating',
        'course'    => 'Course',
        'courses-taught-by' => 'Courses Taught by',
        'enter-something-about-you' => 'Enter something about you',
        'total-earnings' => 'Total Course Earnings',
        'total-affiliate-earnings' => 'Total Affiliate Earnings',
        'manage'    => 'Manage',
        'draft' => 'Draft',
        'under-review' => 'Under Review',
        'live' => 'Live',
        'unpublished' => 'Unpublished',
        'create-first-course' => 'Create your first course to become an Instructor',
        'first-course-intro' => '<p>It is easy to become an Instructor. Simply start by creating your first course to join our community of instructors.</p><p>Use the form below to get started!</p>',
        'create-course' => 'Create Course',
        'course-landing-page'   => 'Course Landing Page',
        'coupons' => 'Coupons',
        'course-price' => 'Course Price',
        'save' => 'Save',
        'free' => 'Free',
        'code'  => 'Code',
        'discount-percentage' => 'Discount Percentage',
        'new-price' => 'New price after coupon is applied',
        'number-of-coupons' => 'Number of Coupons',
        'expiry-date'   => 'Expiry date',
        'optional' => 'Optional',
        'course-coupons' => 'Course Coupons',
        'create-new-coupon' => 'Create New Coupon',
        'cancel' => 'Cancel',
        'create-coupon' => 'Create Coupon',
        'link' => 'Link',
        'percent-off' => 'Percent Off',
        'final-price' => 'Final Price',
        'quantity' => 'Quantity',
        'remaining' => 'Remaining',
        'expires' => 'Expires',
        'status' => 'Status',
        'active' => 'Active',
        'inactive' => 'Inactive',
        'get-link' => 'Get Link',
        'your-coupon-link' => 'Your Coupon Link',
        'close' => 'Close',
        'create' => 'Create',
        'saving' => 'Saving...',
        'updating' => 'Updating...',
        'coupon-saved' => 'Coupon Saved',
        'error-saving-coupon' => 'Error Saving Coupon',
        'updating-coupon-status' => 'Updating coupon status...',
        'status-updated' => 'Status Updated',
        'copied-to-clipboard' => 'Copied to Clipboard',
        'price-updated' => 'Price Updated',
        'course-title' => 'Course title',
        'course-subtitle' => 'Course subtitle',
        'course-category' => 'Course category',
        'course-description' => 'Course description',
        'course-level' => 'Course level',
        'language-of-instruction' => 'Language of Instruction',
        'tags' => 'Tags',
        'update-course' => 'Update Course',
        'course-image' => 'Course Image',
        'image-specifications' => 'Image Specifications',
        'image-specifications-text' => 'The image must be at least 1920x1080px in size for optimal display. Please ensure that the image has minimal text and does not violate any copyright laws. We do not take responsibility or liability for any rights infringements caused by the image you upload, and we reserve the right to reject images submitted for review. File types must be one of png, jpeg, jpg, gif',
    
        'choose-file' => 'Choose File...',
        'create-announcement' => 'Create Announcement',
        'send-announcement' => 'Send Announcement',
        'enter-announcement-content' => 'Enter your announcement content here...',
        'body' => 'Body',
        'title' => 'Title',
        'announcements' => 'Announcements',
        'announcement' => 'Announcement',
        'created-on'    => 'Created On',
        'course-info' => 'Course Info',
        'course-content' => 'Course Content',
        'submit-for-review' => 'Submit for review',
        'publish-course' => 'Publish course',
        'announcements' => 'Announcements',
        'pricing-and-coupons' => 'Pricing and Coupons',
        'unpublish-course' => 'Unpublish course',
        'under-review' => 'Under review',
        'course-curriculum' => 'Course Curriculum',
        'edit-section' => 'Edit section',
        'delete-section' => 'Delete section',
        'lesson' => 'Lesson',
        'edit-lesson' => 'Edit Lesson',
        'delete-lesson' => 'Delete Lesson',
        'manage-content' => 'Manage Content',
        'add-content' => 'Add Content',
        'edit-content' => 'Edit Content',
        'preview-content' => 'Preview in frontend',
        'text-lesson' => 'Text Lesson',
        'video-lesson' => 'Video Lesson',
        'add-resource' => 'Add Lesson Resource',
        'browse-file' => 'Browse file',
        'upload' => 'Upload',
        'upload-msg' => 'Upload resource for this lesson. Only one file is allowed (.zip preferable)',
        'attachment' => 'Attachment',
        'add-video-content' => 'Add Video Content',
        'upload-video' => 'Upload Video',
        'youtube-link' => 'YouTube video link',
        'vimeo-link' => 'Vimeo video link',
        's3-link' => 'Amazon S3 video link',
        'add-text-content' => 'Add Text Content',
        'add-a-lesson' => 'Add a Lesson',
        'add-a-section' => 'Add a Section',
        'are-you-sure' => 'Are you sure?',
        'file-cannot-be-recovered' => 'You will not be able to recover this file again',
        'yes-delete' => 'Yes, Delete it!',
        'deleted' => 'Deleted',
        'deleted-successfully' => 'Item was deleted successfully',
        'cancelled' => 'Cancelled',
        'nothing-deleted' => 'Nothing deleted.',
        'free-preview' => 'Free Preview?',
        'enter-title' => 'Enter title',
        'enter-description' => 'Enter short description',
        'new-lesson' => 'New Lesson',
        'new-section' => 'New Section',
        'section-objective' => 'Section Objective',
        'enter-objective' => 'Enter Section Objective',
        'section-objective-detail' => 'What will students gain from this section?',
        'video-duration' => 'Video Duration',
        'lifetime-earnings' => 'Lifetime Earnings',
        'total-withdrawals' => 'Total Withdrawals',
        'account-balance' => 'Account Balance',
        'submit-withdrawal-request' => 'Submit a withdrawal request. Payments are made via PayPal',
    
        'amount' => 'Amount',
        'paypal-email' => 'PayPal Email',
        'date-submitted' => 'Date Submitted',
        'date-updated' => 'Date Updated',
        'contact-me' => 'Contact Me',
        'comment'   => 'Comment',
        'purchase-details' => 'Purchase Details',
        'purchased-by' => 'Purchased by',
        'your-earning'  => 'Your Earning',
        'withdrawals-payouts' => 'Withdrawals/Payouts',
        'your-current-video' => 'Your current video is:',
        'file-will-be-deleted' => 'This will deleted if you upload a different one.',
        'choose-video' => 'Choose video...',
        'permalink' => 'Permalink',
        'post-answer' => 'Post Answer',
        'enrolled-and-wishlist' => 'My Enrolled Courses, Purchase History and Wishlist',
 'homepage-caption' => 'Arts martiaux, Sports de combats et Yoga',
 'homepage-paragraph' => 'Apprenez à votre rythme, avec les meilleurs instructeurs...',
        'day' => 'day',
        'use-coupon' => 'Use promo code ',
        'off' => 'off',
        'any-course' => 'any course.',
        'expires-in' => 'Expires in',
        'home' => 'Home',
        'start-learning' => 'Start Learning',
        'name' => 'Name',
        'email' => 'Email',
        'message' => 'Message',
        'send' => 'Send',
        'message-sent' => 'Message Sent. We will be in touch shortly',
        'hello-admin' => 'Hello Admin',
        'you-received-a-new-message' => 'You received a new contact message from your site visitor. Here are the details',
        'sender' => 'Sender',
        'content' => 'Content',
        'thanks' => 'Thanks',
        'new-admin-message' => 'New Admin Message',
        'new-announcement' => 'New Announcement',
        'new-announcement-published' => 'A new announcement has been published in ',
        'view-it-here' => 'View it here',
        'new-answer' => 'New answer to question',
        'new-answer-posted' => 'A new answer has been posted to the question',
        'admin-review' => 'Admin Review',
        'admin-approval' => 'Admin Approval',
        'approved' => 'Cours approuvé et en ligne.',
        'disapproved' => 'Cours non approuvé.',
        'reviewed-on' => 'Reviewed on',
        'course-reviewed' => 'Admin has reviewed your course',
        'course-has-been-reviewed' => 'Your course <b>:course</b> has been reviewed by the admin.',
        'connection-timeout' => 'Connection timeout. Try again.',
        'some-error-occured' => 'Some error occured, sorry for the inconvenience',
        'payment-failed' => 'Payment failed / canceled. Try again.',
        'payment-processed' => 'Payment processed successfully. Thank you.',
        'payment-failed' => 'Payment failed. Sorry!',
        'stop-uploading' => 'Stop Uploading',
        'start-uploading' => 'Start Uploading',
        'uploading' => 'Uploading',
        'choose-video' => 'Choose video...',
        'error' => 'Error',
        'size' => 'Size',
        'course-has-no-preview' => 'You must mark at least one video lesson as "FREE PREVIEW" before your course can be approved. Without this the course will not be published',
        'recent-posts' => 'Posts récents',
        'contact-us' => 'Contactez-nous',
    
        'select-payment-method' => 'Sélectionner un moyen de paiement',
        'next' => 'Suivant',
        'previous' => 'Précédent',
        'or' => 'OU',
    
        'promote-this-course' => 'Lien affilié',
        'copy-affiliate-link' => 'Copiez ce lien affilié et partagez pour gagner de l\'argent',
        'min-payout-amount' => 'Le paiement minimum est ',
        'cannot-request-less-than-minimum' => 'You cannot request a payout if your account balance is less than this amount',
        'sales-channel' => 'Sales Channel',
        'organic-sales' => 'Organic Sales',
        'affiliate' => 'Affilié',
        'your-promotion' => 'Votre promotion',
        'withdrawals' => 'Retraits',
        'affiliate-earnings' => 'Revenu affilié',
        'lifetime' => 'A vie',
        'course-sales-revenue' => 'Revenus des ventes du cours',
        'add-question' => 'Ajouter une question',
    
        'quiz' => 'Quiz',
        'you-must-add-one-correct-answer' => 'You must add ONE correct answer before you can save this question',
    
        'course-submitted' => 'New course has been submitted for your review',
        'course-has-been-submitted-for-review' => 'A new course <b>:course</b> has been submitted for your review.',
        'review-it-here' => 'You can review it here',
        'pay-with-razorpay' => 'Pay with Razorpay',
        'processing' => 'Processing...',
        'go-to-razorpay' => 'Go to Razorpay',
        'recommended' => 'Recommandé par iBudokan.academy',
    ],
];
