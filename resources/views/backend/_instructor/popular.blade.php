@extends ('backend.layouts.app')

@section ('title', trans('menus.backend.sidebar.popular-instructors') )

@section('after-styles')
    <style type="text/css">
      .dragArea {
        min-height: 10px;
      }
      .dragme{
        cursor: move;
      }
    </style>
@endsection

@section('page-header')
    <h1>
        {{ trans('menus.backend.sidebar.popular-instructors') }}
        <small>{{ trans('labels.backend.courses.popular-instructors-small') }}</small>
    </h1>
@endsection

@section('content')
    <popular-instructors inline-template>
      <section class="content" v-cloak>
        <div class="row">
            
          <div class="col-md-6">
              <div class="box box-solid" v-cloak>
                <div class="box-header with-border">
                  <h3 class="box-title">{{ trans('menus.backend.sidebar.instructors') }}</h3>
                  <span class="text-warning">{{ trans('labels.backend.courses.drag-here') }}</span>
                </div>
                    <div class="box-body" >
                      <ul class="list-group" >
                        <draggable v-model="instructors" class="dragArea" :options="{group:'instructorlist'}">
                          <li class="list-group-item dragme" v-for="instructor in instructors">
                            <i class="fa fa-bars"></i>
                            <b>@{{instructor.full_name}}</b> 
                              <span class="label label-warning pull-right">
                                  @{{instructor.tagline}}  
                              </span>                              
                                <span class="label label-default pull-right">
                                    courses @{{instructor.courses_count}}/students @{{instructor.students_count}}
                                </span>
                          </li>
                        </draggable>
                      </ul>
                    </div>
                  <!-- /.box-body -->
                  
                  
              </div>
          </div>
          
          <div class="col-md-6">
              <div class="box box-solid">
                  <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('labels.backend.courses.popular-instructors') }}</h3>
                    <span class="text-success">{{ trans('labels.backend.courses.drop-here-instructors') }}</span>
                  </div>
                  <div class="box-body">
                    <ul class="list-group">
                      <draggable v-model="popular_instructors" class="dragArea" :options="{group:'instructorlist'}">
                        <li class="list-group-item dragme" v-for="instructor in popular_instructors">
                          <i class="fa fa-bars"></i>
                          <b>@{{instructor.full_name}}</b>
                          <span class="label label-warning pull-right">
                                  @{{instructor.tagline}}  
                          </span>                          
                                <span class="label label-default pull-right">
                                    courses @{{instructor.courses_count}}/students @{{instructor.students_count}}
                                </span>                          
                        </li>
                      </draggable>
                    </ul>
                    <button type="button" class="btn btn-success" @click.prevent="updatePopularList">{{ trans('labels.backend.courses.update-popular-instructors-list') }}</button>
                    <span class="pull-right" v-show="saveStatus">@{{saveStatus}}</span>
                  </div>
                  <!-- /.box-body -->
              </div>
          </div>
          
          
          
        </div>
        <!-- /.row -->
      </section>
    </popular-instructors>
@endsection