@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management'))

@section('after-styles')

@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management') }}
        <small>{{ trans('labels.backend.access.users.affiliate') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.users.affiliate') }}</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div>
            {{--
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>{{ trans('labels.backend.access.users.table.id') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.first_name') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.last_name') }}</th>
                        <th>{{ trans('labels.backend.access.users.table.sponsor_name') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody style='height:25px;'>
                    @foreach($affiliates as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->first_name }}</td>
                        <td>{{ $item->last_name }}</td>
                        <td><b>id({{ $item->sponsor->id }})</b> {{ $item->sponsor->first_name }} {{ $item->sponsor->last_name }}</td>
                        <td><button class='btn btn-danger'>{{ trans('labels.backend.access.users.table.dissable_sponsor') }}</button></td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            --}}
            @php
            $id = trans('labels.backend.access.users.table.id');
            $fname = trans('labels.backend.access.users.table.first_name');
            $lname = trans('labels.backend.access.users.table.last_name');
            $sponsor = trans('labels.backend.access.users.table.sponsor_name');
            $action = trans('labels.general.actions');
            $ds = trans('labels.backend.access.users.table.dissable_sponsor');
            $confirm = trans('labels.backend.access.users.table.confirm');
            @endphp
            <affiliates 
                v-bind:affiliates="{{$affiliates}}"
                v-bind:header="{
                id:'<?=$id ?>',
                first_name:'<?=$fname ?>',
                last_name:'<?=$lname ?>',
                sponsor:'<?=$sponsor ?>',
                actions:'<?=$action ?>',
                dissable_sponsor:'<?=$ds ?>',
                confirm:'<?=$confirm ?>',
                color:'#fefefe',bg:'#333'
                }">
            </affiliates>

            </div><!--table-responsive-->

        </div><!-- /.box-body -->


    </div><!--box-->

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.users.affiliate-create-relation') }}</h3>
        </div><!-- /.box-header -->


        <div class="box-body">
            <div>
                {{--
                    <table id="users-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ trans('labels.backend.access.users.table.id') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.first_name') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.last_name') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.sponsor_name') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody style='height:25px;'>
                        @foreach($affiliates as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->first_name }}</td>
                            <td>{{ $item->last_name }}</td>
                            <td><b>id({{ $item->sponsor->id }})</b> {{ $item->sponsor->first_name }} {{ $item->sponsor->last_name }}</td>
                            <td><button class='btn btn-danger'>{{ trans('labels.backend.access.users.table.dissable_sponsor') }}</button></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                --}}
                @php
                    $id = trans('labels.backend.access.users.table.id');
                    $fname = trans('labels.backend.access.users.table.first_name');
                    $lname = trans('labels.backend.access.users.table.last_name');
                    $sponsor = trans('labels.backend.access.users.table.sponsor_name');
                    $action = trans('labels.general.actions');
                    $ds = trans('labels.backend.access.users.table.dissable_sponsor');
                    $confirm = trans('labels.backend.access.users.table.confirm');
                    $select = trans('labels.backend.access.users.table.select');
                    $selectParentUser = trans('labels.backend.access.users.table.selectParentUser');
                    $selectChildUser = trans('labels.backend.access.users.table.selectChildUser');
                    $createAffiliation = trans('labels.backend.access.users.table.createAffiliation');
                    $createAffiliateRelation = trans('labels.backend.access.users.table.createAffiliation');
                @endphp
                <affiliate-relations-create
                        v-bind:affiliates="{{$users}}"
                        v-bind:header="{
                                id:'<?=$id ?>',
                                first_name:'<?=$fname ?>',
                                last_name:'<?=$lname ?>',
                                sponsor:'<?=$sponsor ?>',
                                actions:'<?=$action ?>',
                                dissable_sponsor:'<?=$ds ?>',
                                confirm:'<?=$confirm ?>',
                                color:'#fefefe',bg:'#333',
                                select: '<?=$select ?>',
                                selectParentUser: '<?=$selectParentUser ?>',
                                selectChildUser: '<?=$selectChildUser ?>',
                                createAffiliation: '<?=$createAffiliation ?>',
                                createAffiliateRelation: '<?=$createAffiliateRelation ?>'
                                }">
                </affiliate-relations-create>


            </div><!--table-responsive-->

        </div>
    </div>

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.users.affiliate-change-percent') }}</h3>
        </div><!-- /.box-header -->


        <div class="box-body">
            <div>
                {{--
                    <table id="users-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ trans('labels.backend.access.users.table.id') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.first_name') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.last_name') }}</th>
                            <th>{{ trans('labels.backend.access.users.table.sponsor_name') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody style='height:25px;'>
                        @foreach($affiliates as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->first_name }}</td>
                            <td>{{ $item->last_name }}</td>
                            <td><b>id({{ $item->sponsor->id }})</b> {{ $item->sponsor->first_name }} {{ $item->sponsor->last_name }}</td>
                            <td><button class='btn btn-danger'>{{ trans('labels.backend.access.users.table.dissable_sponsor') }}</button></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                --}}
                @php
                    $id = trans('labels.backend.access.users.table.id');
                    $fname = trans('labels.backend.access.users.table.first_name');
                    $lname = trans('labels.backend.access.users.table.last_name');
                    $sponsor = trans('labels.backend.access.users.table.sponsor_name');
                    $affPercent = trans('labels.backend.access.users.table.aff_percent');
                    $action = trans('labels.general.actions');
                    $ds = trans('labels.backend.access.users.table.dissable_sponsor');
                    $confirm = trans('labels.backend.access.users.table.confirm');
                    $change = trans('labels.backend.access.users.table.change');
                    $changeAffiliatePercent = trans('labels.backend.access.users.table.changeAffiliatePercent');
                @endphp

                <affiliate-percent-table
                        v-bind:affiliates="{{$users}}"
                        v-bind:header="{
                                id:'<?=$id ?>',
                                first_name:'<?=$fname ?>',
                                last_name:'<?=$lname ?>',
                                sponsor:'<?=$sponsor ?>',
                                actions:'<?=$action ?>',
                                affiliate_percent: '<?=$affPercent ?>',
                                dissable_sponsor:'<?=$ds ?>',
                                confirm:'<?=$confirm ?>',
                                color:'#fefefe',bg:'#333',
                                change: '<?=$change ?>',
                                changeAffiliatePercent: '<?=$changeAffiliatePercent ?>',
                                }">

                </affiliate-percent-table>
            </div><!--table-responsive-->

        </div>
    </div>




    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('User') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection

@section('after-scripts')
    <script>

    </script>
@endsection

