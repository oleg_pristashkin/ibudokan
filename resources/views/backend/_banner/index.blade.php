@extends ('backend.layouts.app')

@section ('title', trans('strings.backend.manage-banner'))

@section('after-styles')
    <style type="text/css">
        .badge {
            padding: 1px 8px 1px;
            background-color: #aaa !important;
        }
    </style>
@endsection

@section('page-header')
    <h1>{{trans('strings.backend.manage-banner')}}</h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>


            <div class="pull-right">
                <div class="pull-right mb-10">

                </div><!--pull right-->
                <div class="clearfix"></div>
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->
        <div id="fileuploader">Upload</div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="courses-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>{{trans('strings.backend.id')}}</th>
                        <th>{{trans('strings.backend.image')}}</th>
                        <th>{{trans('strings.backend.position')}}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($banners as $banner)
                        <tr>
                            <td>{{ $banner->id }}</td>
                            <td> <img src="{{ $banner->banner_path }}" style="max-width: 200px"> </td>
                            <td>
                                <select class="form-control banner-position" data-id="{{$banner->id}}" style="width: 200px" >
                                    <option  >...</option>
                                    <option {{$banner->banner_position === 'main-up' ? 'selected' : ''}}  value="main-up">{{trans('strings.backend.main-page-up')}}</option>
                                    <option {{$banner->banner_position === 'main-middle' ? 'selected' : ''}}  value="main-middle">{{trans('strings.backend.main-page-middle')}}</option>
                                    <option {{$banner->banner_position === 'main-down' ? 'selected' : ''}}  value="main-down">{{trans('strings.backend.main-page-down')}}</option>
                                    <option {{$banner->banner_position === 'course-up' ? 'selected' : ''}}  value="course-up">{{trans('strings.backend.course-page-up')}}</option>
                                    <option {{$banner->banner_position === 'course-middle' ? 'selected' : ''}}  value="course-middle">{{trans('strings.backend.course-page-middle')}}</option>
                                    <option {{$banner->banner_position === 'course-down' ? 'selected' : ''}}  value="course-down">{{trans('strings.backend.course-page-down')}}</option>
                                </select>
                            </td>
                            <td>


                                {!! Form::open(['route' => ['admin.banners.cleanbanner', 'id' => $banner->id], 'method'=>'POST', 'class' => 'form-horizontal delete-category-form']) !!}


                                <button class="btn btn-xs btn-danger delete">Delete</button>

                                {{ METHOD_FIELD('DELETE') }}

                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->

@endsection

@section('after-scripts')
    <script>

        //confirm delete
        $(".delete-btn").click(function () {
            var slug = $(this).attr('data-slug');
            var lang = $(this).attr('data-lang');
            swal({
                    title: "{{trans('strings.backend.are-you-sure')}}",
                    text: "{{trans('strings.backend.unable-to-recover')}}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('strings.backend.yes-delete')}}",
                    cancelButtonText: "{{trans('strings.backend.cancel')}}",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: 'POST',
                            url: "/admin/pages/" + slug + "/" + lang + "/delete",
                            data: {
                                slug: slug,
                                lang: lang,
                                '_token': '{{csrf_token()}}'
                            },
                            success: function (data) {
                                location.reload();
                            }
                        })
                    } else {
                        swal("{{trans('strings.backend.cancelled')}}", "", "error");
                    }
                });
        });

        /*
        $('button.delete').on('click', function(e){
            e.preventDefault();

            swal({
                title: "{{trans('strings.backend.are-you-sure')}}",
                text: "{{trans('strings.backend.unable-to-recover')}}",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "{{trans('strings.backend.yes-delete')}}",
                closeOnConfirm: false,
                closeOnCancel: false,
                showLoaderOnConfirm: true
            },

            function(isConfirmed){
                if(isConfirmed){
                    setTimeout(function(){
                        swal("{{trans('strings.backend.deleted')}}", "", "success");
                        $(".delete-category-form").submit();
                    }, 2000);
                } else {
                       swal("{{trans('strings.backend.cancelled')}}", "", "error");
                }

            });
        });
        */

    </script>


    <link href="/banner/file.css" rel="stylesheet">

    <script src="/banner/file.js"></script>

    <script>

        $(document).on('change', '.banner-position', function(){
            jQuery.get('/admin/banners/change-position/' + $(this).data('id') + '/' + $(this).val(),
            function () {
                location.reload();
            });

        });

        $(document).ready(function()
        {
            $("#fileuploader").uploadFile({
                url:"/admin/banners/upload",
                fileName:"banner",
                dragDropStr: "<span><b>{{trans('strings.backend.upload-banner')}}</b></span>",
                abortStr:"{{trans('strings.backend.cancelled')}}",
                cancelStr:"résilier",
                doneStr:"fait",
                multiDragErrorStr: "Plusieurs Drag &amp; Drop de fichiers ne sont pas autorisés.",
                extErrorStr:"n'est pas autorisé. Extensions autorisées:",
                sizeErrorStr:"n'est pas autorisé. Admis taille max:",
                uploadErrorStr:"Upload n'est pas autorisé",
                uploadStr:"{{trans('strings.backend.upload-banner')}}",
                onSuccess:function(files,data,xhr,pd)
                {
                    location.reload();
                },
            });
        });
    </script>
@endsection
