@extends('frontend.layouts.app')

@section('title')
    {!! trans('strings.frontend.home') !!}
@endsection

@section('after-styles')

<style type="text/css">
    #background {
        display:none;
        right: 0;
        bottom: 0;
        top: 50%;
        left: 50%;
        width: 100%;
        height: 100%;
        z-index:-100;
        object-fit: cover;
        overflow: hidden;
        border: 1px solid rgba(255,255,255,0.6);
    }

    @media (min-width: 62em) {
       #background {
          display:block;
       }

       .jumbotron .bg-stripe-overlay {
            background: url(/images/bg-jumbotron.png) repeat;
            text-align: left;
            padding: 70px 0;
        }
    }
    .awe-parallax{
        background-color: rgba(17, 83, 140, 0.6);
        z-index:100;
    }

    .button-set-header {
        padding: 10px 25px;
        box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
        border-radius: 5px;
    }

    .button-set-header.one {
        background: rgba(0, 0, 0, 0.2);
        border-bottom: solid 1px rgba(168, 152, 164, 0.65);
        margin: 20px 0 20px;
    }

    .jumbotron h2{
        margin-top:0;
    }

    /* typeahead */
    .jumbotron .search-item{
      color: #ddd;
    }

      .jumbotron .twitter-typeahead,
      .jumbotron .tt-hint,
      .jumbotron .tt-input,
      .jumbotron .tt-menu { width: 100%; }
      .jumbotron .tt-menu.tt-open {margin-top: 0;}
      .jumbotron .tt-query, /* UPDATE: newer versions use tt-input instead of tt-query */
      .jumbotron .tt-hint {
          padding: 8px 12px;
          font-size: 14px;
          line-height: 30px;
          border: 2px solid #ccc;
          border-radius: 4px;
          outline: none;
      }

      .jumbotron .tt-query { /* UPDATE: newer versions use tt-input instead of tt-query */
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
      }

      .jumbotron .tt-hint {
          color: #fff;
      }

      .jumbotron .tt-menu { /* UPDATE: newer versions use tt-menu instead of tt-dropdown-menu */
          min-width: 100%;
          color: #fff !important;
          padding: 0px;
          text-align: left;
          background-color: rgba(0, 0, 0, 0.2);
          border: 1px solid rgba(255, 255, 255, 0.6);
        }


        .jumbotron .tt-menu a {
            color: #d3d3d3 !important;
            font-size: 1.4em;
            line-height: 1.4em;
        }

        .jumbotron .tt-suggestion {
            padding: 3px 10px;
            font-size: 15px;
            line-height: 24px;
        }
         .jumbotron .tt-highlight{
             color: #fff;
         }
        .jumbotron .tt-suggestion.tt-is-under-cursor,
        .jumbotron .tt-suggestion.tt-cursor{
            color: #000;
            background-color: rgba(255, 255,255,0.2);

        }

        .jumbotron #mk-fullscreen-search-input:focus,
        .jumbotron #mk-fullscreen-search-input,
        .jumbotron [type=text].form-control{
            box-shadow: none !important; border: 1px solid #ddd;
            -webkit-box-shadow: none !important; border: 1px solid #ddd;
            border: 1px solid rgba(255,255,255,0.3);
            width: 100%;
        /*    background:rgba(255,255,255,0.2);
            color:#fff; */
        }

        .mc-section-1-content-1 .big {
            margin-bottom: 10px;
        }
        .big {
            font-family: 'Lato', sans-serif;
            font-size: 25px;
            line-height: 1.5em;
            color: #9a9a9a;
            font-weight: bold;
        }
       .one ::-webkit-input-placeholder {
            color:#000 !important;
        }

        .one ::-moz-placeholder {
            color:#000 !important;
        }

        .one ::-ms-placeholder {
            color:#000 !important;
        }

        .one ::placeholder {
            color:#000 !important;
        }
/* ---------------------------------------------------------- */
.jumbotron {
            background-color: #444f58;
            background-size: auto auto;
        }
        .jumbotron h2 {
            font-size: 28px;
        }
        .jumbotron .btn.btn-success,.jumbotron .btn.btn-warning {
            background: #D5BEF4;
            border-width: 3px;
            font-size: 16px;
            padding: 7px 15px;
        }
        .jumbotron .btn.btn-warning {
            background: #A3D29C;
        }

        @php
            $backgrounds = ['background','background1','background2','background3','background4'];
			$m_backgrounds = ['m_background','m_background1','m_background2','m_background3'];
            $bg = $backgrounds[rand(0,4)].'.jpg';
			$m_bg = $m_backgrounds[rand(0,3)].'.jpg';
        @endphp
        .jumbotron-bg {
            background-color: #444f58;
            background-size: auto auto;
            background: url(images/{{ $bg }}) bottom right no-repeat, url(images/{{ $bg }}) repeat-x;
            max-height: 550px; background-size: auto 100%;
        }
	@media (max-width: 48em){ /* 62em old value; optimal value for image (768 x 550)*/
            .jumbotron-bg{
                background-color: #0d568a;
                background: url(images/{{ $m_bg }});background-position-x: center; background-size: auto 100%;
	/* radial-gradient(circle farthest-side at center top,#3ba5e2 0,#0d568a 100%);*/
            }
	}
/* add styles 07.07.19 */
        .mc-section-3 {
            padding: 20px 0 0;
        }
        .mc-section-3 .title-box {
            padding-bottom:12px;
        }
        .feature-slider {
            padding-top: 20px;
        }
        .feature-slider .owl-controls {
            top: 50%;
            z-index: 101;
            position: absolute;
            left: -7px;
            right: -7px;
            width: auto;
        }
        .feature-slider .owl-buttons {
            display: flex;
            justify-content: space-between;
            padding: 0;
            font-size: 38px;
            font-weight: bold;
            line-height: 1;
            position: relative;
            left:0;
            right:0;
            top: 0;
        }
        .feature-slider .owl-buttons div .fa {
            width: 38px;
            background: rgb(255, 255, 255);
            border-radius: 50%;
            color: rgb(0, 119, 145);
            text-align: center;
            border-width: 0px;
            transform: translateY(-50%);
            box-shadow: rgba(20, 23, 28, 0.1) 0px 0px 1px 1px, rgba(20, 23, 28, 0.1) 0px 3px 1px 0px;
            height: 38px;
            line-height: 1;
            font-size: 38px;
            font-weight: 400;
        }
		.feature-slider .owl-buttons div .fa:hover {
                    border-width: 0px;
                    color: rgb(0, 119, 145);
		}
		.mc-item-1:hover .content-item {
		  border-color: #fff !important;
		}
		.mc-item-1:hover .ft-item {
		  border-color: #fff;
		}
        .feature-slider .mc-item-1 {
            box-shadow: 3px 3px 1px #ccc, -1px -1px 1px #ccc;
        }
        @media (max-width: 48em){
            .feature-slider .owl-controls {
                left: -10px;
                right: -10px;
            }
        }
        .feature-course{
            margin-left: -15px;
            margin-right: -15px;
            padding: 0 20px;
        }
</style>
@stop

@section('content')
    {!!  \App\Models\Banner::getBanner('main-up') !!}
    <div class="jumbotron jumbotron-bg" style="z-index: 2;">
        <div class="bg-stripe-overlay">
            <div class="container">
                <div class="row top-row" style='margin:70px 0 120px 0;height:224px;'>
                    <div class="col-md-9 text-center">
                        <h2>{!!trans('strings.frontend.homepage-caption')!!}</h2>
                        <div class="row search-form-box hidden-xs" style="margin-top: 0px;">
                            <div class="well button-set one" style="padding: 10px; border: 0 solid red !important; margin-top:0; width:50%;">
                                <form action="{{ route('frontend.course.get') }}" class="form-horizontal" method="get" id="mk-fullscreen-searchform">
                                    <input type="text" name="search" autocomplete="off" class="main-search form-control input-lg" placeholder="{{trans('strings.frontend.search-author-or-title')}}..." id="mk-fullscreen-search-input">
                                </form>
                            </div>
                        </div>

                        <div class="row">
                            <div class="left">
                                <p>{!!trans('strings.frontend.homepage-paragraph')!!}</p>
                                @if(!auth()->check())
                                    <div class="button-set" style="padding:0;box-shadow:none;">
                                      <a class="btn btn-success" href="{{route('frontend.auth.login')}}">
                                          {!! trans('navs.frontend.login') !!}
                                      </a>
                                      <span class="divider hidden-xs">or</span>
                                      <a href="{{route('frontend.auth.register')}}" class="btn btn-warning">
                                          {!! trans('navs.frontend.register') !!}
                                      </a>
                                    </div>
                                @else
                                      <a class="btn btn-success" href="{{route('frontend.course.get')}}">
                                          {!! trans('strings.frontend.start-learning') !!}
                                      </a>
                                @endif
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    {!!  \App\Models\Banner::getBanner('main-middle') !!}

{{--
    @if(!is_null($coupon))
        <section id="testimonial">
            <div class="container">
                <div class="coupon-info">
                    <h3 class="code orange">{{$coupon->percent}}% {{trans('strings.frontend.off')}}</h3>
                    <h3>{{trans('strings.frontend.any-course')}}</h3>
                    <h3>{{trans('strings.frontend.use-coupon')}}</h3>

                    <h3  class="orange">{{$coupon->code}}</h3>
                    <h3>{{trans('strings.frontend.expires-in')}}</h3>
                    <h3 class="orange"><div id="getting-started"></div></h3>
                </div>
            </div>
        </section>
    @endif


    @if(config('settings.adsense_top_responsive_slot') != '')
        <section class="mc-section-3 section">
            <div class="container">
            <center>
                @if(config('demo.demo_mode'))
                    <div style="max-width: 100%; height: auto; background: rgb(153, 153, 153); color: #fff; line-height: 90px; text-align: center; ">AD BANNER</div>
                @endif
                <!-- Banner Add -->
                <google-adsense
                  ad-client="{{config('settings.adsense_ad_client')}}"
                  ad-slot="{{config('settings.adsense_top_responsive_slot')}}"
                  ad-style="display: block"
                  ad-format="auto">
                </google-adsense>
            </center>
            </div>
        </section>
    @endif
--}}
@if($featuredCategories->count() > 0)
    @foreach($featuredCategories as $category)
        @if(count($category->courses))
            <section class="mc-section-3 section">
                <div class="container">
                    <!-- FEATURE -->
                    <div class="feature-course">
                        <h4 class="title-box text-uppercase">{{trans('strings.frontend.featured-in')}} <b><a href="{{route('frontend.course.get')}}?category={{$category->slug}}">{{$category->name}}</a></b></h4>

                        <div class="spinner">
                            <center>
                                <i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw text-muted"></i>
                            </center>
                        </div>
                        <div class="row">
                            <div class="feature-slider owl-theme" style="display:none">

                                @foreach($category->courses as $course)
                                        <course-card
                                        v-bind:course="{{ $course }}"
                                        currency-symbol="{{config('settings.currency_symbol')}}"
                                        promote-course="{{trans('strings.frontend.promote-this-course')}}"
                                        copy-affiliate-link="{{trans('strings.frontend.copy-affiliate-link')}}"
                                        domain="{{ Request::root() }}"
                                        total_video_duration="{{\App\Models\Course::getTotalVideoDuration($course->slug)}}"
                                        ></course-card>
                                @endforeach
                            </div>
                        </div>


                    </div>
                    <!-- END / FEATURE -->
                </div>
            </section>
        @endif
            <!-- END / SECTION 3 -->
    @endforeach
@endif

<!-- Best Courses -->
<fbest-courses
        title-box="{{trans('strings.frontend.best')}}"
        currency-symbol="{{config('settings.currency_symbol')}}"
        promote-course="{{trans('strings.frontend.promote-this-course')}}"
        copy-affiliate-link="{{trans('strings.frontend.copy-affiliate-link')}}"
        modal-close="{{trans('strings.frontend.close')}}"
        domain="{{ Request::root() }}"
        carousel-items="5" carousel-items-desktop="[1199,5]">
</fbest-courses>

<!-- Recommended Courses -->
<frecommended-courses
    title-box="{{trans('strings.frontend.recommended')}}"
    currency-symbol="{{config('settings.currency_symbol')}}"
    promote-course="{{trans('strings.frontend.promote-this-course')}}"
    copy-affiliate-link="{{trans('strings.frontend.copy-affiliate-link')}}"
    modal-close="{{trans('strings.frontend.close')}}"
    domain="{{ Request::root() }}"
    carousel-items="5" carousel-items-desktop="[1199,5]">
</frecommended-courses>

<!-- END Recommended Courses -->

    <!-- SECTION 1 -->
    <section id="mc-section-1" style="padding:25px 0px;" class="mc-section-1 section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <h2 class="big">{{trans('strings.frontend.recent-posts')}}</h2>
                        @foreach($posts as $p)
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="featured-item">
                                        <h4 class="title-box text-uppercase">
                                            <a href="{{route('frontend.posts.show', $p)}}">{{$p->title}}</a>
                                        </h4>
                                        <p>
                                        {!! str_limit(strip_tags($p->body), 120) !!}
                                        <span><a href="{{route('frontend.posts.show', $p)}}">{{trans('strings.frontend.read-more')}} <i class="fa fa-angle-double-right"></i></a></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- END / SECTION 1 -->
<fpopular-instructors
    title-box="{{trans('strings.frontend.popular')}}"
    t-courses="{{trans('strings.frontend.courses')}}"
    t-participants="{{trans('strings.frontend.participants')}}"
    carousel-items="5" carousel-items-desktop="[1199,5]">
</fpopular-instructors>
    {!!  \App\Models\Banner::getBanner('main-down') !!}
    @if(config('settings.adsense_top_responsive_slot') != '')
        <section id="mc-section-3" class="mc-section-3 section">
            <div class="container">
            <center>
                @if(config('demo.demo_mode'))
                    <div style="max-width: 100%; height: auto; background: rgb(153, 153, 153); color: #fff; line-height: 90px; text-align: center; ">AD BANNER</div>
                @endif
                <!-- Banner Add -->
                <google-adsense
                  ad-client="{{config('settings.adsense_ad_client')}}"
                  ad-slot="{{config('settings.adsense_top_responsive_slot')}}"
                  ad-style="display: block"
                  ad-format="auto">
                </google-adsense>
            </center>
            </div>
        </section>
    @endif
    <input type="hidden" value="" id="days-left"/>

 <popular-tags header="{{trans('strings.frontend.popular-tags')}}"></popular-tags>

@endsection
@section('after-scripts')
    @if(!is_null($coupon))
        <script type="text/javascript">
            $('#getting-started').countdown('{{$coupon->expires}}', function(event) {
                $(this).html(event.strftime("%D {{str_plural(trans('strings.frontend.day'), $coupon->days_left)}} %H:%M:%S"));
            });
        </script>
    @endif

    <script type="text/javascript">
        $(document).ready(function(){
            $('.search-form-box').fadeIn(100);
        });
//
//
//        function copyToClipboard(el){
//            document.querySelector(el).select();
//            document.execCommand('copy');
//            $('.feedback-message').text("{{trans('strings.frontend.copied-to-clipboard')}}");
//            setTimeout(() => {
//               $('.feedback-message').text('');
//            }, 3000);
//
//        }
    </script>



@endsection