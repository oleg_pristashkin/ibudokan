@extends('frontend.layouts.app')

@section('content')
@section('after-styles')
<style type="text/css">
@media (min-width: 992px){
    .col-md-20 {
        width: 20%;
    }
}
@media (max-width: 767px){
    .row {
        justify-content: center;
    }
}
</style>
@stop
    @include('frontend.user.course._top')
        
    <section id="categories-content" class="categories-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-push-0" style="padding-top:15px;">
                    
                        <div class="row" style="margin-bottom: 20px;display: flex;flex-wrap: wrap;">
                            @forelse($courses as $course)
                                <course-card 
                                    m_class="col-12 col-sm-4 col-md-20"
                                    m_style="padding:0;margin-bottom: 25px;min-width:205px;"
                                    v-bind:course="{{ $course }}"
                                    currency-symbol="{{config('settings.currency_symbol')}}"
                                    promote-course="{{trans('strings.frontend.promote-this-course')}}"
                                    copy-affiliate-link="{{trans('strings.frontend.copy-affiliate-link')}}"
                                    modal-close="{{trans('strings.frontend.close')}}"
                                    total_video_duration="{{\App\Models\Course::getTotalVideoDuration($course->slug)}}"
                                    domain="{{ Request::root() }}">                                            
                                </course-card>
                            {{--    @include('frontend.user.course._course') --}}
                            @empty
                                <center>
                                    <p class="lead">
                                        {{trans('strings.frontend.nothing-in-wishlist')}}
                                        
                                    </p>
                                    <p class="lead">
                                        <a href="{{route('frontend.course.get')}}">{{trans('strings.frontend.browse-and-add-to-wishlist')}}</a>
                                    </p>
                                    
                                </center>
                            @endforelse
                        </div><!-- end row -->                    
                </div>
            </div>
        </div>
    </section>
    <!-- END / CATEGORIES CONTENT -->
@endsection