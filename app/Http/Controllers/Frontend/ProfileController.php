<?php

namespace App\Http\Controllers\Frontend;

use Helper;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function show(Request $request, User $user)
    {
        $courses = Course::where('user_id', $user->id)->where('published', true)->where('approved', true);
        $courses = $courses->hasContent()->get();
        //$courses = $user->authored_courses->where('published', true)->where('approved', true);
        foreach($courses as $course){
            $course->image = Helper::coverImage($course);
            $course->url = "/courses/" . $course->slug . "/learn";
            $course->authorPicture = $course->author->picture;
            $course->authorUrl = "/user/" . $course->author->username;
            $course->affiliate = (Auth::user() && $course->price>0);
            $course->affiliateLink = (Auth::user())?(Auth::user()->affiliate_id):0;
            $course->categoryUrl = $course->category->slug;
            $course->categoryName = $course->category->name;
        }
        return view('frontend.user.public.profile', compact('user', 'courses'));
    }
    
}
