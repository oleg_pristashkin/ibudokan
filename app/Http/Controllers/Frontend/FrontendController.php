<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Agent;
use Helper;
use App\Models\Blog;
use Carbon\Carbon;
use App\Models\Lesson;
use App\Models\Course;
use App\Models\Coupon;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\Auth;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        
        // make sure to only return categories that actually have courses with content, and at least one content should be a video preview

        $featuredCategories = Category::has('courses')
            ->has('courses.sections')
            ->has('courses.sections.lessons')
            ->has('courses.sections.lessons.content')
            ->with(['courses' => function($q){
                $q->where('featured', true)
                    ->whereHas('sections', function($q){
                    $q->whereHas('lessons', function($q){
                        $q->where('preview', true)
                            ->whereHas('content', function($q){
                                $q->where('content_type', '=', 'video');
                            });
                    });
            });
        }])->get();
                //->orderByRaw("RAND()")->paginate(3);

        foreach($featuredCategories as $category){
            
            foreach($category->courses as $course){
                $course->image = Helper::coverImage($course);
                $course->url = "/courses/" . $course->slug . "/learn";
                $course->authorPicture = $course->author->picture;
                $course->authorUrl = "/user/" . $course->author->username;
                $course->affiliate = (Auth::user() && $course->price>0);
                $course->affiliateLink = (Auth::user())?(Auth::user()->affiliate_id):0;
                $course->categoryUrl = $course->category->slug;
                $course->categoryName = $course->category->name;
            }
        }
        
        /*
        $featuredCourses = Course::where('featured', true)
            ->whereHas('sections', function($q){
                $q->whereHas('lessons', function($q){
                    $q->where('preview', true)
                        ->whereHas('content', function($q){
                            $q->where('content_type', '=', 'video');
                        });
                });
            })->get();
        
        foreach($featuredCourses as $course){
            $course->image = Helper::coverImage($course);
        }
        */
        
//        $recommendedCourses = Course::where('recommended', true)->get();
//        foreach($recommendedCourses as $course){
//                $course->image = Helper::coverImage($course);
//            }
            
        $coupon = Coupon::where(['sitewide' => true, 'active'=> true ])->where('expires', '>=', Carbon::now())->first();
        
        
        if(!is_null($coupon)){
            $expires = Carbon::parse($coupon->expires);
            $coupon->days_left = $expires->diffInDays(Carbon::now());
        }
        
        $posts = Blog::latest()->where(['type' => 'article', 'featured_frontend' => true])->get()->take(2);
        $featured_page = Blog::where(['type' => 'page', 'featured_frontend' => true])->get()->first();
        
        return view('frontend.index', compact('featuredCategories', 'posts', 'coupon', 'featured_page', 'recommendedCourses'));

    }

    /**
     * @return \Illuminate\View\View
     */
    public function macros()
    {
        return view('frontend.macros');
    }
    
    public function fetchRecommended(Request $request)
    { 
        $courses = Course::with("category")->whereHas('category', function($q){
            $q->orderBy('name');
        })
        ->where('recommended', true)
        ->get();
        foreach($courses as $course){
                $course->image = Helper::coverImage($course);
                $course->url = "/courses/" . $course->slug . "/learn";
                $course->authorPicture = $course->author->picture;
                $course->authorUrl = "/user/" . $course->author->username;
                $course->affiliate = (Auth::user() && $course->price>0);
                $course->affiliateLink = (Auth::user())?(Auth::user()->affiliate_id):0;
            }
        return $courses->toArray();
    }

    public function fetchBest(Request $request)
    {
        $courses = Course::with("category")->whereHas('category', function($q){
            $q->orderBy('name');
        })
            ->where('best', true)
            ->get();
        foreach($courses as $course){
            $course->image = Helper::coverImage($course);
            $course->url = "/courses/" . $course->slug . "/learn";
            $course->authorPicture = $course->author->picture;
            $course->authorUrl = "/user/" . $course->author->username;
            $course->affiliate = (Auth::user() && $course->price>0);
            $course->affiliateLink = (Auth::user())?(Auth::user()->affiliate_id):0;
        }
        return $courses->toArray();
    }


    public function fetchPopular()
    {
       $instructors = User::with('courses')->has('courses')->where('popular',true)->get();
        $allInstructors = [];
        $i = 0;
        foreach($instructors as $item){
          $allInstructors[$i]['id'] = $item->id;  
          $allInstructors[$i]['full_name'] = $item->full_name;
          $allInstructors[$i]['tagline'] = $item->tagline;
          $allInstructors[$i]['userUrl'] = "/user/" . $item->username;
          $allInstructors[$i]['picture'] = $item->picture;
          $allInstructors[$i]['courses_count'] = $item->courses->count();
          $allInstructors[$i]['students_count'] = 0;
          foreach($item->courses as $course){
            $allInstructors[$i]['students_count'] += $course->students->count();
          }
          $i++;
        }
        return $allInstructors;
    }
    
    public function fetchTags()
    {
        $used_tags = DB::table('taggable_tags')
	    		->join('taggable_taggables', 'taggable_tags.tag_id', '=', 'taggable_taggables.tag_id')
	    		->join('courses', 'courses.id', '=', 'taggable_taggables.taggable_id')
	    		->groupBy('taggable_tags.normalized')
	    		->groupBy('taggable_tags.name')
	    		->orderBy('tag_count', 'DESC')
	    		->limit(20)
	    		->get(['taggable_tags.name', 'taggable_tags.normalized', DB::raw('count(taggable_tags.normalized) as tag_count')] );
        $tags = [];
        $i = 0;
        foreach($used_tags as $item){
            if(!empty($item->name)){
                $tags[$i]['name'] = strToUpper($item->name) . ' (' . $item->tag_count . ')';
                $tags[$i]['href'] = route('frontend.course.tag.get', strToLower($item->name));
                $i++;
            }
        }
        
        
        return $tags;
    }
}
