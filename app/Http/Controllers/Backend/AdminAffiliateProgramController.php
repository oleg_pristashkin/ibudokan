<?php

namespace App\Http\Controllers\Backend;

use App\Models\Access\User\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;

/**
 * Description of AdminAffiliateProgramController
 *
 * @author Олег
 */
class AdminAffiliateProgramController extends Controller
{
    public function sponsors(Request $request)
    {
        /*
        $users = User::with('sponsor')
                       ->where('affiliate_id','=','40852')
                       ->get();
        foreach($users as $item){
        }
        */
        return view('backend.affiliate.sponsors');
    }

    public function affiliates(Request $request)
    {
        $users = User::with('sponsor')
            ->where('ibudokan_affid', '!=', '0')
            ->get();

        $allUsers = User::all();


        $affiliates = [];
        $allUsersSorted = [];
        $i = 0;

        foreach ($users as $item) {
            $affiliates[$i]['id'] = (string)$item->id;
            $affiliates[$i]['first_name'] = $item->first_name;
            $affiliates[$i]['last_name'] = $item->last_name;
            $affiliates[$i]['sponsor'] = '"' . $item->sponsor->username . '" ' . $item->sponsor->first_name . ' ' . $item->sponsor->last_name;
            $i++;
        }

        $i = 0;
        foreach ($allUsers as $item) {
            $allUsersSorted[$i]['id'] = (string)$item->id;
            $allUsersSorted[$i]['first_name'] = $item->first_name;
            $allUsersSorted[$i]['last_name'] = $item->last_name;
            $allUsersSorted[$i]['affiliate_percent'] = $item->affiliate_percent;
            $i++;
        }
        return view('backend.affiliate.affiliates', [
            'affiliates' => json_encode($affiliates),
            'users' => json_encode($allUsersSorted)
        ]);
    }

    public function sponsorDissable(Request $request)
    {
        try {
            User::where('id', $request->id)
                ->update(['ibudokan_affid' => 0]);
        } catch (\Exception $e) {
            return response()->json(null, 404);
        }
        return response()->json(null, 200);
    }

    public function relationCreate(Request $request)
    {
        try {
            $sponsor = User::where('id', $request->sponsor_id)
                ->first();

            if ($sponsor->id === (int)$request->sponsored_id) {
                throw new \Exception();
            }
            if ($request->sponsored_id === 0 || $request->sponsor_id === 0) {
                throw new \Exception();
            }
            User::where('id', $request->sponsored_id)
                ->update(['ibudokan_affid' => $sponsor->affiliate_id]);
        } catch (\Exception $e) {
            return response()->json(null, 404);
        }
        return response()->json(null, 200);
    }

    public function updateAffiliatePercent(Request $request)
    {
        try {
            User::where('id', $request->id)
                ->update(['affiliate_percent' => $request->percent]);
        } catch (\Exception $e) {
            return response()->json(null, 404);
        }
        return response()->json(null, 200);
    }
}
