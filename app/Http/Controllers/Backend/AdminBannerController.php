<?php

namespace App\Http\Controllers\Backend;

use App\Jobs\UploadVideo;
use App\Models\Banner;
use App\Models\Blog as Post;
use App\Models\BlogCategory as Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Exception;


class AdminBannerController extends Controller
{
    public function index(Request $request)
    {
        $banners = Banner::all();

        return view('backend._banner.index', ['banners' => $banners]);
    }



    public function setPosition(Request $request)
    {
        $banner = Banner::find($request->id);
        $banner->banner_position = $request->position;
        $oldBanner = Banner::where('banner_position', $request->position)->first();
        if($oldBanner){
            $oldBanner->banner_position = '';
            $oldBanner->save();
        }

        $banner->save();
        return response()->json(['status' => 'success'], 200);
    }

    public function uploadBanner(Request $request)
    {

        $originalFileName = $request->file('banner')->getClientOriginalName();
        $ext = $request->file('banner')->extension();

        $filename = str_random(3) . '-' . substr(str_slug($originalFileName), 0, -3) . '.' . $ext;
        try {
            $path = $request->file('banner')->storeAs('uploads', $filename, 'tmpStorage');

            $this->dispatch(new UploadVideo(
                $filename
            ));

        } catch (Exception $e) {
            report($e);
        }

        $banner = new Banner();

        if (config('settings.uploadLocation') == 's3') {
            $banner->banner_path = Storage::disk('s3')->url($filename);
        } else {
            $banner->banner_path = '/uploads/videos/' . $filename;
        }

        $banner->save();
        return response()->json(['status' => 'success'], 200);

    }


    public function cleanBanner(Request $request)
    {

        $banner = Banner::find($request->id);

        $banner->delete();
        return redirect()->back();
    }


}
