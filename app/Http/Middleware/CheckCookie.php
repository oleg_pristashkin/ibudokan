<?php

namespace App\Http\Middleware;

use Illuminate\Http\Response;
use Closure;

class CheckCookie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // check if no cookie is attached to the incoming request and if it has a ref id, set the cookie before moving on.
        if( !$request->hasCookie('ibudokan_affid') && $request->query('ref') ) {
            return redirect($request->fullUrl())->withCookie(cookie('ibudokan_affid', $request->query('ref'), config('settings.affiliate_cookie_lifetime')));
        }
        return $next($request);
    }
}
