<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public static function getBanner($name)
    {
        $banner = self::where('banner_position', $name)->first();

        if ($banner) {
            return "<img style='display:block; width:100%; margin:0 auto;' src='" . $banner->banner_path . "' />";
        }
        return '';
    }
}
