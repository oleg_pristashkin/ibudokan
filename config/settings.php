<?php

return [
    // colors can be green, blue, red, olive, grey
    'site_theme'   => 'grey',
    
    'uploadLocation' => 's3',
    'minimumPayoutAmount' => '50',
    'affiliateSalesPercentage' => '5',
    'organicSalesPercentage' => '50',
    'promoSalesPercentage' => '50',
    'max_video_upload_size' => '1000',
    'send_emails' => true,
    'site_description' => 'Site description in english',
    'google_analytics' => 'UA-XXXXX-X',
    'currency_code' => 'EUR',
    'currency_symbol' => 'Є',
    'currency_symbol_position' => 'front',
    
    'contact_email'     => 'info@ibudokan.academy',
    'social_facebook'   => null,
    'social_twitter'    => null,
    'social_google'     => null,
    'social_youtube'    => null,

    'allow_video_upload' => true,
    'allow_youtube_video' => false,
    'allow_vimeo_video'  => false,
    'allow_s3_video'     => false,
    'pricelist' => '9,19,29,39,49,59,69,79,89,99,129,149',
    
    
    'adsense_ad_client' => 'ca-pub-1722028856965661',
    'adsense_sidebar_responsive_slot' => '5558331441',
    'adsense_top_responsive_slot' => '5410991781',
    
    // cookie lifetime in minutes
    'affiliate_cookie_lifetime' => '1440',
    
    'enable_paypal' => true,
    'enable_stripe' => true,
    'enable_braintree' => false,
    'enable_omise' => false,
    'enable_razorpay' => false,
    

];